read -p "How many keys?: " -e keys
read -p "Server IP: " -e server
read -p "Server Port: " -e port
read -p "Server Network Interface: " -e iface

server_privkey=$(wg genkey)
server_pubkey=$(echo $server_privkey | wg pubkey)

echo "
[Interface]
Address = 10.50.50.1/24, fd50:50:50::1/64
PostUp = iptables -t nat -A POSTROUTING -o $iface -j MASQUERADE; ip6tables -t nat -A POSTROUTING -o $iface -j MASQUERADE
PostDown = iptables -t nat -D POSTROUTING -o $iface -j MASQUERADE; ip6tables -t nat -D POSTROUTING -o $iface -j MASQUERADE
ListenPort = $port
PrivateKey = $server_privkey
" > wg-server.conf

for i in $(seq 2 $(( $keys+1 )) ); do
	privkey=$(wg genkey)
	pubkey=$(echo $privkey | wg pubkey)
    x=$(printf '%x' $i)
	echo "
[Interface]
PrivateKey = $privkey
Address = 10.50.50.$i/32, fd50:50:50::$x/64
DNS = 8.8.8.8

[Peer]
PublicKey = $server_pubkey
Endpoint = $server:$port
AllowedIPs = 0.0.0.0/0, ::/0
" > wg-client-$(( $i-1 )).conf

echo "
[Peer]
PublicKey = $pubkey
AllowedIPs = 10.50.50.$i/32, fd50:50:50::$x/64
" >> wg-server.conf

done
